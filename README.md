# ATM Store

ATM Store was developed in the frame of the collaborative tutored project of Master 2 ICE.
ATM Store allows to monitor and manage applications deployment (status, installation, uninstallation or update) on remote servers with an user-friendly user interface, easily accessible through a web browser.

## How to install it ?

On the web page, click on the download icon.
Select an archive format. Once the archive is downloaded, extract it to the desired folder.
for more information check the user documentation or the technical documentation

## How to run it ?

Open a terminal and travel to your folder where you extracted ATM Store
Then execute the following commands :

- _python manage.py makemigrations_
- _python manage.py migrate_
- _python manage.py runserver_

## Requirements
**To use / install the app you must have the following dependencies:**
- Git
- Python (3.6.3 is the minimum)
- pip


**and the following python packages** 
- asgiref (3.3.1 or higher)
- configparser2 (4.0.0 or higher)
- cycler (0.10.0 or higher)
- dj-database-url (0.5.0 or higher)
- Django (3.1.5 or higher)
- django-heroku (0.3.1 or higher)
- gunicorn (20.0.4 or higher)
- paramiko (2.7.2 or higher)
- psycopg2 (2.8.6 or higher)
- pytz (2021.1 or higher)
- six (1.15.0 or higher)
- sqlparse (0.4.1 or higher)
- whitenoise (5.2.0 or higher)


## Who are the contributors ?
 Adrien Stora, Emma Dos Santos, Gilles Labrousse, Brian Grillot, Bastien Didier, Kamal Aarab, Mathieu Olives Delmas, Thomas Palis

## See ATM Store - User documentation.pdf
## See ATM Store - Technical documentation.pdf

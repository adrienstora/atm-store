import json
from home.models import Application


def parser_local_settings(response_localsettings, response_status):
    response = "".join(response_localsettings)
    a = response.split("\n}")[0]
    b = response_status[0].split("\n")[0]
    a += ",\n  \"status\":\"" + b + "\"\n}"

    return a


def parse_local_settings(response_localsettings, response_status, usernameServer, ipServer, portServer, pathServer):
    a = parser_local_settings(response_localsettings, response_status)
    obj = json.loads(a)

    if Application.objects.filter(ip=ipServer).count() == 0 & Application.objects.filter(
            name=obj["APP_NAME"]).count() == 0 & Application.objects.filter(
            emplacement=pathServer).count() == 0:
        app = Application(name=obj["APP_NAME"], username=usernameServer, version=obj["APP_VERSION"],
                          ip=ipServer, port=portServer, emplacement=pathServer)
        app.save()
        return a
    else:
        return "The application is already in the database"


def parse_update_local_settings(response_localsettings, response_status, appId):
    a = parser_local_settings(response_localsettings, response_status)
    obj = json.loads(a)
    appLocal = Application.objects.get(id=appId)
    appLocal.version = obj["APP_VERSION"]
    appLocal.name = obj["APP_NAME"]

    appLocal.save()
    return a


def parse_local_settings_update(response_localsetting, response_update):
    response = "".join(response_localsetting)
    a = response.split("\n}")[0]
    b = response_update[1].split("\n")[0]
    a += ",\n  \"resultat\":\"" + b + "\"\n}"

    return a


def parse_local_settings_install(response_localsetting, response_install):
    response = "".join(response_localsetting)
    a = response.split("\n}")[0]
    b = response_install[1].split("\n")[0]
    a += ",\n  \"resultat\":\"" + b + "\"\n}"

    return a


def parse_local_settings_uninstall(response_localsetting, response_uninstall):
    response = "".join(response_localsetting)
    a = response.split("\n}")[0]
    b = response_uninstall[1].split("\n")[0]
    a += ",\n  \"resultat\":\"" + b + "\"\n}"

    return a

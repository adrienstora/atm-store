import paramiko as paramiko

from home.parsers import parse_local_settings, parse_local_settings_install, parse_local_settings_update, \
    parse_local_settings_uninstall, parse_update_local_settings


def connection(ip, port, username, password, command):
    lines = []
    try:
        ssh = paramiko.SSHClient()

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        ssh.connect(ip, port, username, password, timeout=5)

        stdin, stdout, stderr = ssh.exec_command(command, timeout=5)

        lines = stdout.readlines()
    except Exception as e:
        lines.append("Timeout")

    return lines


def get_install(ip, port, username, password, path):
    response_localsetting = connection(ip, port, username, password, "cat "+path+"/.local_settings")
    response_install = connection(ip, port, username, password, "cd " + path + "| ./app install")
    if response_localsetting[0] == "Timeout":
        return response_localsetting[0]
    return parse_local_settings_install(response_localsetting, response_install)


def get_update(ip, port, username, password, path):
    response_localsetting = connection(ip, port, username, password, "cat "+path+"/.local_settings")
    response_update = connection(ip, port, username, password, "cd " + path + "| ./app update")
    if response_localsetting[0] == "Timeout":
        return response_localsetting[0]
    return parse_local_settings_update(response_localsetting, response_update)


def get_uninstall(ip, port, username, password, path):
    response_localsetting = connection(ip, port, username, password, "cat "+path+"/.local_settings")
    response_uninstall = connection(ip, port, username, password, "cd " + path + "| ./app uninstall")
    if response_localsetting[0] == "Timeout":
        return response_localsetting[0]
    return parse_local_settings_uninstall(response_localsetting, response_uninstall)


def get_local_settings(ip, port, username, password, path):
    response_localsetting = connection(ip, port, username, password, "cat "+path+"/.local_settings")
    response_status = connection(ip, port, username, password, "cd " + path + "| ./app status")

    if len(response_localsetting) > 0 and response_localsetting[0] == "Timeout":
        return response_localsetting[0]
    elif not response_localsetting:
        return "badPath"
    return parse_local_settings(response_localsetting, response_status, username, ip, port, path)


def update_local_settings(ip, port, username, password, path, idapp):
    response_localsetting = connection(ip, port, username, password, "cat "+path+"/.local_settings")

    response_status = connection(ip, port, username, password, "cd " + path + "| ./app status")

    if len(response_localsetting) > 0 and response_localsetting[0] == "Timeout":
        return response_localsetting[0]
    elif not response_localsetting:
        return "badPath"
    return parse_update_local_settings(response_localsetting, response_status, idapp)

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from home.sshconect import get_local_settings, get_install, get_update, get_uninstall, update_local_settings

from home.models import Application


def home(request):
    user = ""
    connected = 0
    User.objects.all().delete()
    user = User.objects.create_user(username='toto', password='titi')
    user.save()

    if request.session.has_key("user"):
        user_id = request.session["user"]
        connected = 1
    else:
        User.objects.all().delete()
        user = User.objects.create_user(username='toto', password='titi')
        user.save()
        Application.objects.all().delete()
        app2 = Application.objects.create(version="1", name="ATM Store", port=22, ip="137.74.141.82", username="am",
                                          emplacement="~")
        app2.save()
        app3 = Application.objects.create(version="1", name="Monitoring", port=22, ip="137.74.143.82", username="a",
                                          emplacement="~")
        app3.save()
        app4 = Application.objects.create(version="3", name="Failures DB", port=22, ip="137.74.144.82", username="m",
                                          emplacement="~")
        app4.save()

    list_app = Application.objects.all()

    context = {
        'listApplication': list_app,
        'connected': connected
    }

    return render(request, "templates/home/render.html", context)


def login_user(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                request.session.create()
                request.session["user"] = user.id
                request.session.set_expiry(86400)
                login(request, user)  # the user is now logged in
                return HttpResponse(user, status="200")
            else:
                return HttpResponse("No User", status="500")

        else:
            return HttpResponse("No User", status="500")


def logout_user(request):
    request.session.delete("serverPassword")
    request.session.delete("idApplication")
    logout(request)
    request.session.delete("user")
    request.session.flush()
    return HttpResponse("Ok", status="200")


def local_settings(request):
    ip = request.POST.get('ip')
    port = request.POST.get('port')
    username = request.POST.get('serverUsername')
    password = request.POST.get('serverPassword')
    path = request.POST.get('path')
    return check_timeout(get_local_settings(ip, port, username, password, path))


def install(request):
    password = request.session["serverPassword"]
    idApplication = request.session["idApplication"]
    app = Application.objects.get(id=idApplication)
    return check_timeout(get_install(app.ip, app.port, app.username, password, app.emplacement))


def update(request):
    password = request.session["serverPassword"]
    idApplication = request.session["idApplication"]
    app = Application.objects.get(id=idApplication)
    return check_timeout(get_update(app.ip, app.port, app.username, password, app.emplacement))


def uninstall(request):
    password = request.session["serverPassword"]
    idApplication = request.session["idApplication"]
    app = Application.objects.get(id=idApplication)
    return check_timeout(get_uninstall(app.ip, app.port, app.username, password, app.emplacement))


def connectToServer(request):
    password = request.POST.get('serverPassword')
    idApplication = request.POST.get('idApplication')

    application = Application.objects.get(id=idApplication)
    result = check_timeout(
        update_local_settings(application.ip, application.port, application.username, password, application.emplacement,
                              idApplication))

    if result.status_code == 200:
        request.session["serverPassword"] = password
        request.session["idApplication"] = idApplication
    return result


def check_timeout(reponse):
    if reponse == "Timeout":
        return HttpResponse(reponse, status=502)
    return HttpResponse(reponse, status=200)



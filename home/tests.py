from django.contrib.auth.models import User
from django.test import Client
from django.test import TestCase

from home.models import Application


class ApplicationTestCase(TestCase):
    def setUp(self):
        Application.objects.all().delete()
        app1 = Application.objects.create(version="1", name="ATM STORE", port=22, ip="137.71.148.82", username="at",
                                          emplacement="~")
        app1.save()
        app2 = Application.objects.create(version="1", name="fb", port=22, ip="137.74.141.82", username="am",
                                          emplacement="~")
        app2.save()
        app3 = Application.objects.create(version="1", name="book", port=22, ip="137.74.143.82", username="a",
                                          emplacement="~")
        app3.save()
        app4 = Application.objects.create(version="3", name="fk", port=22, ip="137.74.144.82", username="m",
                                          emplacement="~")
        app4.save()

    def test_creation_application(self):
        """Animals that can speak are correctly identified"""
        app1 = Application.objects.get(name="ATM STORE")
        app2 = Application.objects.get(name="fb")
        app3 = Application.objects.get(name="book")
        app4 = Application.objects.get(name="fk")
        self.assertEqual(app1.port, '22')
        self.assertEqual(app2.port, '22')
        self.assertEqual(app3.port, '22')
        self.assertEqual(app4.port, '22')

        self.assertEqual(app1.version, "1")
        self.assertEqual(app2.version, "1")
        self.assertEqual(app3.version, "1")
        self.assertEqual(app4.version, "3")

        self.assertEqual(app1.ip, "137.71.148.82")
        self.assertEqual(app2.ip, "137.74.141.82")
        self.assertEqual(app3.ip, "137.74.143.82")
        self.assertEqual(app4.ip, "137.74.144.82")

        self.assertEqual(app1.username, "at")
        self.assertEqual(app2.username, "am")
        self.assertEqual(app3.username, "a")
        self.assertEqual(app4.username, "m")


class clientTestCase(TestCase):
    def setUp(self):
        self.c = Client()
        User.objects.all().delete()
        user = User.objects.create_user(username='toto', password='titi')
        user.save()
        Application.objects.all().delete()
        app1 = Application.objects.create(version="1", name="ATM STORE", port=22, ip="137.71.148.82", username="at",
                                          emplacement="~")
        app1.save()
        app2 = Application.objects.create(version="1", name="fb", port=22, ip="137.74.141.82", username="am",
                                          emplacement="~")
        app2.save()
        app3 = Application.objects.create(version="1", name="book", port=22, ip="137.74.143.82", username="a",
                                          emplacement="~")
        app3.save()
        app4 = Application.objects.create(version="3", name="fk", port=22, ip="137.74.144.82", username="m",
                                          emplacement="~")
        app4.save()

    def test_connection(self):
        response = self.c.post('/login', {'username': 'toto', 'password': 'titi'})
        self.assertEqual(response.status_code, 200)

        response = self.c.get('/home')
        self.assertEqual(response.status_code, 200)

        response = self.c.post('/infoapp', {"ip": "137.74.148.82", "port": "22", "serverUsername": "atm", "serverPassword": "atmstore", "path": "~"})
        self.assertEqual(response.status_code, 200)
        apps = Application.objects.get(username="atm")
        response = self.c.post('/connectToServer',
                          { "serverPassword": "atmstore",
                            "idApplication": str(apps.id)})
        self.assertEqual(response.status_code, 200)

        response = self.c.get('/logout')
        self.assertEqual(response.status_code, 200)

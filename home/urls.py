from django.urls import path

from . import views

urlpatterns = [
    path('logout', views.logout_user, name='logout'),
    path('home', views.home, name='ATM Store'),
    path('login', views.login_user, name='login'),
    path('', views.home, name='ATM Store'),
    path('infoapp', views.local_settings, name='ATM Store'),
    path('install', views.install, name='ATM Store'),
    path('update', views.update, name='ATM Store'),
    path('uninstall', views.uninstall, name='ATM Store'),
    path('connectToServer', views.connectToServer, name='Connect To Server')
]

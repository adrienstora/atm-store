var selectedServer = -1;

document.querySelectorAll('*[id^="btnServer"]').forEach(button => {
    button.addEventListener('click', event => {
        selectedServer = button.id;
        console.info("Server " + selectedServer + " selected");
    });
});



var serverAdress = "http://atm-store.herokuapp.com"
var lastApplicationIdConnected = 0

function addServer() {
    var ip = $("#ip").val();
    var port = $("#port").val();
    var serverUsername = $("#serverUsername").val();
    var serverPassword = $("#serverPassword").val();
    var path = $("#path").val();

    $.post(serverAdress + "/infoapp", {
        ip: ip,
        port: port,
        serverUsername: serverUsername,
        serverPassword: serverPassword,
        path: path
    })
        .done(function (appinfo) {
            window.location.reload();
        })
        .fail(function () {
            alert("error");
        });
}


function connectToServer() {
    var serverPassword = $("#confirmPassword").val();
    var idApplication = $(".modal-body #idApplication").val()

    $.post(serverAdress + "/connectToServer", {
        serverPassword: serverPassword,
        idApplication: idApplication
    })
        .done(function (appinfo) {
            //alert(appinfo);

            if (lastApplicationIdConnected != 0){
                disconnect(lastApplicationIdConnected)
            }
            console.log("modify")
            lastApplicationIdConnected = idApplication
            document.getElementById("connectToServer_" + idApplication).hidden = true;
            document.getElementById("disconnectServer_" + idApplication).hidden = false;
            $("#serverConnectModal").modal("hide");
            if (appinfo == "badPath"){
                 document.getElementById("buttonInstall_" + idApplication).hidden = false;
            }
            if (appinfo != "badPath"){
            document.getElementById("buttonUpdate_" + idApplication).hidden = false;
            document.getElementById("buttonUninstall_" + idApplication).hidden = false;
            }

        })
        .fail(function () {
            alert("error");
        });
}

function disconnect(idApplication){
    console.log("disconnect")
    lastApplicationIdConnected = 0
    document.getElementById("connectToServer_" + idApplication).hidden = false;
    document.getElementById("disconnectServer_" + idApplication).hidden = true;
    console.log(document.getElementById("button_" + idApplication))
    document.getElementById("buttonInstall_" + idApplication).hidden = true;
    document.getElementById("buttonUpdate_" + idApplication).hidden = true;
    document.getElementById("buttonUninstall_" + idApplication).hidden = true;

}


function installApplication(idApplication){
     console.log("install");
     document.getElementById("spinnerRunning").hidden = false;
     document.getElementById("mainDiv").style.pointerEvents = "none";

     $.post(serverAdress + "/install", {
        idApplication: idApplication
    })
        .done(function (appinfo) {
            console.log(appinfo)
            document.getElementById("spinnerRunning").hidden = true;
            document.getElementById("mainDiv").style.pointerEvents = "auto";
            updateVariableApplication(idApplication, appinfo)

              setTimeout(function() {
                alert("Application installed")
              },100)

        })
        .fail(function () {
            document.getElementById("spinnerRunning").hidden = true;
            document.getElementById("mainDiv").style.pointerEvents = "auto";
            setTimeout(function() {
                alert("Error")
            },100)
        });
}

function updateApplication(idApplication){
    console.log("update");
     document.getElementById("spinnerRunning").hidden = false;
     document.getElementById("mainDiv").style.pointerEvents = "none";

    $.post(serverAdress + "/update", {
        idApplication: idApplication
    })
        .done(function (appinfo) {
            document.getElementById("spinnerRunning").hidden = true;
            document.getElementById("mainDiv").style.pointerEvents = "auto";
            updateVariableApplication(idApplication, appinfo)

              setTimeout(function() {
                   alert("Application updated")
              },100)
        })
        .fail(function () {
            alert("error");
        });


}

function uninstallApplication(idApplication){
    console.log("uninstall");

     document.getElementById("spinnerRunning").hidden = false;
     document.getElementById("mainDiv").style.pointerEvents = "none";

    $.post(serverAdress + "/uninstall", {
        idApplication: idApplication
    })
        .done(function (appinfo) {
            console.log(appinfo);
            document.getElementById("spinnerRunning").hidden = true;
            document.getElementById("mainDiv").style.pointerEvents = "auto";
            updateVariableApplication(idApplication, appinfo)
              setTimeout(function() {
                   alert("Application uninstalled")
              },100)
        })
        .fail(function () {
            alert("error");
        });


}


function updateVariableApplication(idApplication, appinfo){

     var app = JSON.parse(appinfo);
     console.log(app)
     console.log(app['APP_NAME'])
     document.getElementById("appname_" + idApplication).textContent=app['APP_NAME'];

     console.log(app['APP_NAME'])
     document.getElementById("appversion_" + idApplication).textContent=app['APP_VERSION'];

}

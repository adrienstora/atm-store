$(document).ready(function () {
    console.log("ready");
})



var serverAdress = "http://atm-store.herokuapp.com"


/**
 * Function triggered on click on sign in button
 */
function onSignIn() {
    signIn();
}

/**
 * Function triggered on click on sign out button
 */
function onSignOut() {
    signOut();
}

function signIn() {
    var login = $("#login").val();
    var password = $("#password").val()
    /*$.post("http://127.0.0.1:8000/login", { username: login, password: password })
        .done(function (user) {
            window.location.reload();
        })
        .fail(function () {
            document.getElementById("loginInvalid").hidden = false;
        })*/
    $.post(serverAdress + "/login", { username: login, password: password })
        .done(function (user) {
            window.location.reload();
        })
        .fail(function () {
            document.getElementById("loginInvalid").hidden = false;
        })

}

function signOut() {
    /*$.post("http://127.0.0.1:8000/logout")
    .done(function () {
        window.location.reload();
    });*/
    $.post(serverAdress + "/logout")
    .done(function () {
        window.location.reload();
    });

}